﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Authentication::InitFirebase()
extern void Authentication_InitFirebase_mC3A91A4A38AF6113C11348D469C051C89D0E5F1D (void);
// 0x00000002 System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Authentication::SignUp(System.String,System.String)
extern void Authentication_SignUp_m329D9467C59C49579666C36ED5CD43ED01998E57 (void);
// 0x00000003 System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Authentication::SignIn(System.String,System.String)
extern void Authentication_SignIn_m3C6C86B388290696C58BD309A3CC41CA310EC072 (void);
// 0x00000004 System.Threading.Tasks.Task Authentication::ResetPassword(System.String)
extern void Authentication_ResetPassword_m293F64AF58615E653A38628752B88F32D4C536AC (void);
// 0x00000005 System.Void Current::Start()
extern void Current_Start_m634101D8F61E4B80E68F2F22068B92599DCC57DE (void);
// 0x00000006 System.Void Current::Click()
extern void Current_Click_m90E55AEDDD77BAE72F10A31B3F72C527FAE3A51F (void);
// 0x00000007 Note Current::FindCurrent()
extern void Current_FindCurrent_mBBAC5EE705914552721775DB48E069D754BF957B (void);
// 0x00000008 System.Void Current::.ctor()
extern void Current__ctor_m1227E3CA3875AA1104D032714AF9F5F7C61B7669 (void);
// 0x00000009 System.Void Current::.cctor()
extern void Current__cctor_mAE867A36574908C83C39E9B7660B492E2B1B9548 (void);
// 0x0000000A System.Void FireBaseInit::Start()
extern void FireBaseInit_Start_m7B6EA9D28EB2CBBC78D31C25D9836A6226B286DB (void);
// 0x0000000B System.Void FireBaseInit::.ctor()
extern void FireBaseInit__ctor_mE292D130312957AF791950A1EA9A60B7688FF0F1 (void);
// 0x0000000C System.Void FireBasesStruct::.cctor()
extern void FireBasesStruct__cctor_mD88AE7362A20F63380C2B5FF97A0A5D4E850053A (void);
// 0x0000000D System.Void FireBasesStruct::Combine(System.Text.StringBuilder&,System.String[])
extern void FireBasesStruct_Combine_mE48BDCF354E8E295F880DF3F396741E9A154DB21 (void);
// 0x0000000E T FireBasesStruct::Deserialize(Firebase.Database.DataSnapshot)
// 0x0000000F System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot> FireBasesStruct::Get(System.String[])
extern void FireBasesStruct_Get_m0D90330BBE93533F95E579D3A7F10CF022E00E6D (void);
// 0x00000010 System.Threading.Tasks.Task FireBasesStruct::Save(System.String,System.String[])
extern void FireBasesStruct_Save_m8D7D87330AC081ED048473CC186494BA76D3AC01 (void);
// 0x00000011 System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot> FireBasesStruct::SearchNote(System.String,System.String)
extern void FireBasesStruct_SearchNote_mEEF92184171C83393C4549909A9F12CF16CDF32F (void);
// 0x00000012 System.Void ManagerUI::Start()
extern void ManagerUI_Start_m9758FA56677CD4DB277EA841859CA2B1BCF142F4 (void);
// 0x00000013 System.Boolean ManagerUI::GetWritten(System.String&,UnityEngine.UI.InputField)
extern void ManagerUI_GetWritten_mC1393D1711EC3DB5197832E2218DF511B65E1220 (void);
// 0x00000014 System.Void ManagerUI::FromLoginToNotes()
extern void ManagerUI_FromLoginToNotes_m15F467D1EC75ADA0EE57E4B385D402C81C0C8514 (void);
// 0x00000015 System.Void ManagerUI::ToSort()
extern void ManagerUI_ToSort_m55D566E72C3813EDB8948E563C02CEE7DD4FBB41 (void);
// 0x00000016 System.Void ManagerUI::ClearScrolledContent()
extern void ManagerUI_ClearScrolledContent_m8CD8D3B77F44A58139314043C18B6FF82FDA222C (void);
// 0x00000017 System.Void ManagerUI::Exit()
extern void ManagerUI_Exit_mD13461D4C0C3FADFC6CB2102B6AA5021B8DD5B86 (void);
// 0x00000018 System.Void ManagerUI::.ctor()
extern void ManagerUI__ctor_m125F3B16E61D1033C4DB2F5B6E5E2CC15AB9F632 (void);
// 0x00000019 User NetworkControll::get_User()
extern void NetworkControll_get_User_m5EA9F6FD8BE53C687F8380B672AAA69C5C6ACDD3 (void);
// 0x0000001A System.String NetworkControll::get_Uid()
extern void NetworkControll_get_Uid_mECC698B1D18C25DBCF1249EC698E4DCC8CF35D86 (void);
// 0x0000001B System.Void NetworkControll::set_Uid(System.String)
extern void NetworkControll_set_Uid_m55747E273063DC45A6113FC4D1FD2641876C9E69 (void);
// 0x0000001C System.Void NetworkControll::Awake()
extern void NetworkControll_Awake_m307B72E985C7F6309A7FB3E3BE59171DD6D249D2 (void);
// 0x0000001D System.Void NetworkControll::Login()
extern void NetworkControll_Login_mED49B32F1DA57B57998044904F5192A6DD3D49AF (void);
// 0x0000001E System.Void NetworkControll::CreateNewUser()
extern void NetworkControll_CreateNewUser_m95320682060C9BDD9A7D0B076BD7B058AC2C176B (void);
// 0x0000001F System.Void NetworkControll::CreateNote()
extern void NetworkControll_CreateNote_m5278F6C2692EE0A978FD45EA1D976C5ED4DDDA24 (void);
// 0x00000020 System.Void NetworkControll::EditNote()
extern void NetworkControll_EditNote_mEA7B8ED38157BA0C314E1C3DE3EC7BC05D8A082F (void);
// 0x00000021 System.Void NetworkControll::DeleteNote()
extern void NetworkControll_DeleteNote_m34141C6039D9E2BC502F15463749FD95F5EE306A (void);
// 0x00000022 System.Void NetworkControll::UpdateData()
extern void NetworkControll_UpdateData_m1673EF5C5A6771DE80BDF14203ED4322F7997EE7 (void);
// 0x00000023 System.Void NetworkControll::.ctor()
extern void NetworkControll__ctor_m02D8CCA30EC86626F56DC5CA90689A9211444BC2 (void);
// 0x00000024 System.String Note::get_Title()
extern void Note_get_Title_m0C4815C04CB6D6AD2B98D6213D831B960F799FD5 (void);
// 0x00000025 System.Void Note::set_Title(System.String)
extern void Note_set_Title_m913167355D7E5572EC50376D181FE10D214DD46D (void);
// 0x00000026 System.String Note::get_Description()
extern void Note_get_Description_m3B373E019CC04E1EF1E8B9CCEDA18416C5FFD688 (void);
// 0x00000027 System.Void Note::set_Description(System.String)
extern void Note_set_Description_m575CE4983418D0EFF4BE2F8F014EC554347A19BE (void);
// 0x00000028 System.String Note::get_Date()
extern void Note_get_Date_m86D4C86AA4B6F515D20FFED50A040AE05CA9D444 (void);
// 0x00000029 System.Void Note::set_Date(System.String)
extern void Note_set_Date_mBD28A3310C615770A1FFE2DCADB37E9B52CA798C (void);
// 0x0000002A System.String Note::get_Uid()
extern void Note_get_Uid_mDD19B7D00C2589D3BD4BA52C7EA72E7783DCFBBD (void);
// 0x0000002B System.Void Note::.ctor(System.String,System.String)
extern void Note__ctor_mEC4F960172BBDAAD6E6267C2662A61864E3A83E9 (void);
// 0x0000002C System.String Note::ToString()
extern void Note_ToString_m78B4818496FBB09866862C67E882AEA39B3F9FD7 (void);
// 0x0000002D System.Void NotePref::Start()
extern void NotePref_Start_mE4AE76A77AB21B804BDA6A22F7FBA0F3B152CDC5 (void);
// 0x0000002E System.Void NotePref::CreateNotePrefab(Note)
extern void NotePref_CreateNotePrefab_m61E43CCA886DAFE82667A541B601D75649751101 (void);
// 0x0000002F System.Void NotePref::ShowNotes(System.Collections.Generic.List`1<Note>)
extern void NotePref_ShowNotes_mA021B9E5CEB0EBFB2865E17D8E5EE243FFC6C251 (void);
// 0x00000030 System.Void NotePref::ShowNotes(System.Linq.IOrderedEnumerable`1<Note>)
extern void NotePref_ShowNotes_mFADA77A7FA4797D5F8CDF845693486EB657C0821 (void);
// 0x00000031 System.Void NotePref::.ctor()
extern void NotePref__ctor_m2126F75580F489746F28A19725B038874A8BBBCF (void);
// 0x00000032 System.Void Search::add_Searching(System.Action`1<System.Collections.Generic.List`1<Note>>)
extern void Search_add_Searching_mD8C9263A6503CED34674FC049D984E00BC34CC97 (void);
// 0x00000033 System.Void Search::remove_Searching(System.Action`1<System.Collections.Generic.List`1<Note>>)
extern void Search_remove_Searching_m992F799426C44C0C7E7587F76714751115410467 (void);
// 0x00000034 System.Void Search::Awake()
extern void Search_Awake_m18767DA5D7BD05B1AE0718FA5F3F6C3C9ED4C2E6 (void);
// 0x00000035 System.Void Search::OnChanged()
extern void Search_OnChanged_mF8D4C22919619DD37563A135C55C761A11B4E70B (void);
// 0x00000036 System.Void Search::OnEndEdit()
extern void Search_OnEndEdit_m288EDE5338E098D3CCEEBDE331DDEDDEB29CA41A (void);
// 0x00000037 System.Void Search::Filter()
extern void Search_Filter_mE0FE24BED8620BAF68CF7FE4BA21625250D43B73 (void);
// 0x00000038 System.Void Search::.ctor()
extern void Search__ctor_m647ADFA9634745FA4F654DCBA63DBF7EBF9794A5 (void);
// 0x00000039 System.Void Sort::OrderByTitle()
extern void Sort_OrderByTitle_m55C6E6B83EE73B5EF5C94047DCA55C64B5B6832E (void);
// 0x0000003A System.Void Sort::OrderByDate()
extern void Sort_OrderByDate_m20576967A270E526AD22D6B592EE5778982616F7 (void);
// 0x0000003B System.Void Sort::.ctor()
extern void Sort__ctor_mBA800DB4F489FB1C99637F4A40768364A2935D43 (void);
// 0x0000003C System.Void ImageManager::PickImage()
extern void ImageManager_PickImage_mA0A1884E740590E1A041F8C600943E30BCBC6C8B (void);
// 0x0000003D System.Void ImageManager::.ctor()
extern void ImageManager__ctor_mA8FF9D6E6A187316FD5FDDC90891E4A991C260BB (void);
// 0x0000003E System.Void ImageManager::<PickImage>b__1_0(System.String)
extern void ImageManager_U3CPickImageU3Eb__1_0_m99834472D3C2F20D961E75B21BF4C0A875FDA1E0 (void);
// 0x0000003F System.Void UploadImage::Start()
extern void UploadImage_Start_m57A22DF60A3CE08E3AC0E6CEACA62A5089B1AFD7 (void);
// 0x00000040 System.Void UploadImage::StartUpload(UnityEngine.Texture2D)
extern void UploadImage_StartUpload_mBAF7D4C94E43A7A6E40C852DE1632C481994C52C (void);
// 0x00000041 System.Collections.IEnumerator UploadImage::UploadCoroutine(UnityEngine.Texture2D)
extern void UploadImage_UploadCoroutine_mFC11D7F668B480B5A4526AAAB880D90DFB285463 (void);
// 0x00000042 System.Void UploadImage::.ctor()
extern void UploadImage__ctor_m25D0559241001E4AC2AD38B2C3C4A9845062ADB8 (void);
// 0x00000043 System.Void User::.ctor(System.String)
extern void User__ctor_mCE8FC86126E383AB040A8ADC37E8A5A056986FE2 (void);
// 0x00000044 System.String User::GetUid()
extern void User_GetUid_m4EDA9155BF57BAEC90B7AC441566C7A11D645980 (void);
// 0x00000045 System.Void User::AddNote(Note)
extern void User_AddNote_m585771C31201721C836C47892360BFE65995C21A (void);
// 0x00000046 System.Void User::EditNote(Note)
extern void User_EditNote_m052AD2FEC8CDD3C8737D70DA4B7D9164C19FEAA8 (void);
// 0x00000047 System.Void User::DeleteNote(Note)
extern void User_DeleteNote_m580E6EDA0BB3594A3656A740A9603F9687ACAE5B (void);
// 0x00000048 System.String User::ToString()
extern void User_ToString_m00946220E2DB0D418B5CC21F1FAFE08995F9DFE4 (void);
// 0x00000049 System.Void FireBaseInit/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m357301D12E426DA23DF15B4B820B249847DD8710 (void);
// 0x0000004A System.Void FireBaseInit/<Start>d__1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__1_SetStateMachine_m0A3D213E6669417204AB1327A25A17FA9B1033D2 (void);
// 0x0000004B System.Void NetworkControll/<Login>d__11::MoveNext()
extern void U3CLoginU3Ed__11_MoveNext_mCE16EF6D49949C97AE4199486E80B1AF860F1455 (void);
// 0x0000004C System.Void NetworkControll/<Login>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoginU3Ed__11_SetStateMachine_m0B507F1355CC9A08B7719EBADF2A26FEC82944E6 (void);
// 0x0000004D System.Void NetworkControll/<CreateNewUser>d__12::MoveNext()
extern void U3CCreateNewUserU3Ed__12_MoveNext_m177CF75FA5C9DF2E8F2381350700DE63D1F18644 (void);
// 0x0000004E System.Void NetworkControll/<CreateNewUser>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateNewUserU3Ed__12_SetStateMachine_m4258A64835CC6D8DC790485A789CF3550A071F7E (void);
// 0x0000004F System.Void NetworkControll/<>c::.cctor()
extern void U3CU3Ec__cctor_m6B3E8B6921410AA4729925824BE8870E5DAC564C (void);
// 0x00000050 System.Void NetworkControll/<>c::.ctor()
extern void U3CU3Ec__ctor_mDC862BA9A21E53DA6A724D8058F1AD5F0EE990A4 (void);
// 0x00000051 System.Void NetworkControll/<>c::<UpdateData>b__16_0(System.Threading.Tasks.Task)
extern void U3CU3Ec_U3CUpdateDataU3Eb__16_0_m84A9C0FAE271B6D60B32A2D4176A505A013EFD42 (void);
// 0x00000052 System.Void Search/<Filter>d__8::MoveNext()
extern void U3CFilterU3Ed__8_MoveNext_m46782994E523B25A90B31D53F18DC6FE3B2C9AA6 (void);
// 0x00000053 System.Void Search/<Filter>d__8::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CFilterU3Ed__8_SetStateMachine_mD5F68C9ECEFC120530129A44C7AE78D164B027DC (void);
// 0x00000054 System.Void Sort/<>c::.cctor()
extern void U3CU3Ec__cctor_m3FB417C6BE31C16D2CEFE11C6519E400BC21EE21 (void);
// 0x00000055 System.Void Sort/<>c::.ctor()
extern void U3CU3Ec__ctor_m09082DD18EF55E56529B8B492685DE53AFA42748 (void);
// 0x00000056 System.String Sort/<>c::<OrderByTitle>b__0_0(Note)
extern void U3CU3Ec_U3COrderByTitleU3Eb__0_0_mF33CBB1CB03DE8DCA2606D6B04DBA5C5F0E8784C (void);
// 0x00000057 System.String Sort/<>c::<OrderByDate>b__1_0(Note)
extern void U3CU3Ec_U3COrderByDateU3Eb__1_0_mB4A45AE2B84AFE601EE6A92FD407A3C8B028D193 (void);
// 0x00000058 System.Void UploadImage/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m8F6FAFBFA00627D9CBB17B9051846F0F30A0974F (void);
// 0x00000059 System.Boolean UploadImage/<>c__DisplayClass3_0::<UploadCoroutine>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CUploadCoroutineU3Eb__0_m838C2736A775D8D7F2724C16C8876263F3A76901 (void);
// 0x0000005A System.Void UploadImage/<UploadCoroutine>d__3::.ctor(System.Int32)
extern void U3CUploadCoroutineU3Ed__3__ctor_m95335C80E62EB6BE48507F309F000BD3E23DB11D (void);
// 0x0000005B System.Void UploadImage/<UploadCoroutine>d__3::System.IDisposable.Dispose()
extern void U3CUploadCoroutineU3Ed__3_System_IDisposable_Dispose_m9B092E9A2C6CC4BFAF7DF5AD15CADFB42E0E578A (void);
// 0x0000005C System.Boolean UploadImage/<UploadCoroutine>d__3::MoveNext()
extern void U3CUploadCoroutineU3Ed__3_MoveNext_m3EAB7574A694091C8460147135112CA52861CC7F (void);
// 0x0000005D System.Object UploadImage/<UploadCoroutine>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUploadCoroutineU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3566843B6A72F6FEAEB8283CE651C40CE72DC81C (void);
// 0x0000005E System.Void UploadImage/<UploadCoroutine>d__3::System.Collections.IEnumerator.Reset()
extern void U3CUploadCoroutineU3Ed__3_System_Collections_IEnumerator_Reset_m6780C1046D1E07C2C66DB71815F6BFA511978ECD (void);
// 0x0000005F System.Object UploadImage/<UploadCoroutine>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CUploadCoroutineU3Ed__3_System_Collections_IEnumerator_get_Current_m38D8C3F79A6E04528D70D138A7C69ABBB4CB2907 (void);
static Il2CppMethodPointer s_methodPointers[95] = 
{
	Authentication_InitFirebase_mC3A91A4A38AF6113C11348D469C051C89D0E5F1D,
	Authentication_SignUp_m329D9467C59C49579666C36ED5CD43ED01998E57,
	Authentication_SignIn_m3C6C86B388290696C58BD309A3CC41CA310EC072,
	Authentication_ResetPassword_m293F64AF58615E653A38628752B88F32D4C536AC,
	Current_Start_m634101D8F61E4B80E68F2F22068B92599DCC57DE,
	Current_Click_m90E55AEDDD77BAE72F10A31B3F72C527FAE3A51F,
	Current_FindCurrent_mBBAC5EE705914552721775DB48E069D754BF957B,
	Current__ctor_m1227E3CA3875AA1104D032714AF9F5F7C61B7669,
	Current__cctor_mAE867A36574908C83C39E9B7660B492E2B1B9548,
	FireBaseInit_Start_m7B6EA9D28EB2CBBC78D31C25D9836A6226B286DB,
	FireBaseInit__ctor_mE292D130312957AF791950A1EA9A60B7688FF0F1,
	FireBasesStruct__cctor_mD88AE7362A20F63380C2B5FF97A0A5D4E850053A,
	FireBasesStruct_Combine_mE48BDCF354E8E295F880DF3F396741E9A154DB21,
	NULL,
	FireBasesStruct_Get_m0D90330BBE93533F95E579D3A7F10CF022E00E6D,
	FireBasesStruct_Save_m8D7D87330AC081ED048473CC186494BA76D3AC01,
	FireBasesStruct_SearchNote_mEEF92184171C83393C4549909A9F12CF16CDF32F,
	ManagerUI_Start_m9758FA56677CD4DB277EA841859CA2B1BCF142F4,
	ManagerUI_GetWritten_mC1393D1711EC3DB5197832E2218DF511B65E1220,
	ManagerUI_FromLoginToNotes_m15F467D1EC75ADA0EE57E4B385D402C81C0C8514,
	ManagerUI_ToSort_m55D566E72C3813EDB8948E563C02CEE7DD4FBB41,
	ManagerUI_ClearScrolledContent_m8CD8D3B77F44A58139314043C18B6FF82FDA222C,
	ManagerUI_Exit_mD13461D4C0C3FADFC6CB2102B6AA5021B8DD5B86,
	ManagerUI__ctor_m125F3B16E61D1033C4DB2F5B6E5E2CC15AB9F632,
	NetworkControll_get_User_m5EA9F6FD8BE53C687F8380B672AAA69C5C6ACDD3,
	NetworkControll_get_Uid_mECC698B1D18C25DBCF1249EC698E4DCC8CF35D86,
	NetworkControll_set_Uid_m55747E273063DC45A6113FC4D1FD2641876C9E69,
	NetworkControll_Awake_m307B72E985C7F6309A7FB3E3BE59171DD6D249D2,
	NetworkControll_Login_mED49B32F1DA57B57998044904F5192A6DD3D49AF,
	NetworkControll_CreateNewUser_m95320682060C9BDD9A7D0B076BD7B058AC2C176B,
	NetworkControll_CreateNote_m5278F6C2692EE0A978FD45EA1D976C5ED4DDDA24,
	NetworkControll_EditNote_mEA7B8ED38157BA0C314E1C3DE3EC7BC05D8A082F,
	NetworkControll_DeleteNote_m34141C6039D9E2BC502F15463749FD95F5EE306A,
	NetworkControll_UpdateData_m1673EF5C5A6771DE80BDF14203ED4322F7997EE7,
	NetworkControll__ctor_m02D8CCA30EC86626F56DC5CA90689A9211444BC2,
	Note_get_Title_m0C4815C04CB6D6AD2B98D6213D831B960F799FD5,
	Note_set_Title_m913167355D7E5572EC50376D181FE10D214DD46D,
	Note_get_Description_m3B373E019CC04E1EF1E8B9CCEDA18416C5FFD688,
	Note_set_Description_m575CE4983418D0EFF4BE2F8F014EC554347A19BE,
	Note_get_Date_m86D4C86AA4B6F515D20FFED50A040AE05CA9D444,
	Note_set_Date_mBD28A3310C615770A1FFE2DCADB37E9B52CA798C,
	Note_get_Uid_mDD19B7D00C2589D3BD4BA52C7EA72E7783DCFBBD,
	Note__ctor_mEC4F960172BBDAAD6E6267C2662A61864E3A83E9,
	Note_ToString_m78B4818496FBB09866862C67E882AEA39B3F9FD7,
	NotePref_Start_mE4AE76A77AB21B804BDA6A22F7FBA0F3B152CDC5,
	NotePref_CreateNotePrefab_m61E43CCA886DAFE82667A541B601D75649751101,
	NotePref_ShowNotes_mA021B9E5CEB0EBFB2865E17D8E5EE243FFC6C251,
	NotePref_ShowNotes_mFADA77A7FA4797D5F8CDF845693486EB657C0821,
	NotePref__ctor_m2126F75580F489746F28A19725B038874A8BBBCF,
	Search_add_Searching_mD8C9263A6503CED34674FC049D984E00BC34CC97,
	Search_remove_Searching_m992F799426C44C0C7E7587F76714751115410467,
	Search_Awake_m18767DA5D7BD05B1AE0718FA5F3F6C3C9ED4C2E6,
	Search_OnChanged_mF8D4C22919619DD37563A135C55C761A11B4E70B,
	Search_OnEndEdit_m288EDE5338E098D3CCEEBDE331DDEDDEB29CA41A,
	Search_Filter_mE0FE24BED8620BAF68CF7FE4BA21625250D43B73,
	Search__ctor_m647ADFA9634745FA4F654DCBA63DBF7EBF9794A5,
	Sort_OrderByTitle_m55C6E6B83EE73B5EF5C94047DCA55C64B5B6832E,
	Sort_OrderByDate_m20576967A270E526AD22D6B592EE5778982616F7,
	Sort__ctor_mBA800DB4F489FB1C99637F4A40768364A2935D43,
	ImageManager_PickImage_mA0A1884E740590E1A041F8C600943E30BCBC6C8B,
	ImageManager__ctor_mA8FF9D6E6A187316FD5FDDC90891E4A991C260BB,
	ImageManager_U3CPickImageU3Eb__1_0_m99834472D3C2F20D961E75B21BF4C0A875FDA1E0,
	UploadImage_Start_m57A22DF60A3CE08E3AC0E6CEACA62A5089B1AFD7,
	UploadImage_StartUpload_mBAF7D4C94E43A7A6E40C852DE1632C481994C52C,
	UploadImage_UploadCoroutine_mFC11D7F668B480B5A4526AAAB880D90DFB285463,
	UploadImage__ctor_m25D0559241001E4AC2AD38B2C3C4A9845062ADB8,
	User__ctor_mCE8FC86126E383AB040A8ADC37E8A5A056986FE2,
	User_GetUid_m4EDA9155BF57BAEC90B7AC441566C7A11D645980,
	User_AddNote_m585771C31201721C836C47892360BFE65995C21A,
	User_EditNote_m052AD2FEC8CDD3C8737D70DA4B7D9164C19FEAA8,
	User_DeleteNote_m580E6EDA0BB3594A3656A740A9603F9687ACAE5B,
	User_ToString_m00946220E2DB0D418B5CC21F1FAFE08995F9DFE4,
	U3CStartU3Ed__1_MoveNext_m357301D12E426DA23DF15B4B820B249847DD8710,
	U3CStartU3Ed__1_SetStateMachine_m0A3D213E6669417204AB1327A25A17FA9B1033D2,
	U3CLoginU3Ed__11_MoveNext_mCE16EF6D49949C97AE4199486E80B1AF860F1455,
	U3CLoginU3Ed__11_SetStateMachine_m0B507F1355CC9A08B7719EBADF2A26FEC82944E6,
	U3CCreateNewUserU3Ed__12_MoveNext_m177CF75FA5C9DF2E8F2381350700DE63D1F18644,
	U3CCreateNewUserU3Ed__12_SetStateMachine_m4258A64835CC6D8DC790485A789CF3550A071F7E,
	U3CU3Ec__cctor_m6B3E8B6921410AA4729925824BE8870E5DAC564C,
	U3CU3Ec__ctor_mDC862BA9A21E53DA6A724D8058F1AD5F0EE990A4,
	U3CU3Ec_U3CUpdateDataU3Eb__16_0_m84A9C0FAE271B6D60B32A2D4176A505A013EFD42,
	U3CFilterU3Ed__8_MoveNext_m46782994E523B25A90B31D53F18DC6FE3B2C9AA6,
	U3CFilterU3Ed__8_SetStateMachine_mD5F68C9ECEFC120530129A44C7AE78D164B027DC,
	U3CU3Ec__cctor_m3FB417C6BE31C16D2CEFE11C6519E400BC21EE21,
	U3CU3Ec__ctor_m09082DD18EF55E56529B8B492685DE53AFA42748,
	U3CU3Ec_U3COrderByTitleU3Eb__0_0_mF33CBB1CB03DE8DCA2606D6B04DBA5C5F0E8784C,
	U3CU3Ec_U3COrderByDateU3Eb__1_0_mB4A45AE2B84AFE601EE6A92FD407A3C8B028D193,
	U3CU3Ec__DisplayClass3_0__ctor_m8F6FAFBFA00627D9CBB17B9051846F0F30A0974F,
	U3CU3Ec__DisplayClass3_0_U3CUploadCoroutineU3Eb__0_m838C2736A775D8D7F2724C16C8876263F3A76901,
	U3CUploadCoroutineU3Ed__3__ctor_m95335C80E62EB6BE48507F309F000BD3E23DB11D,
	U3CUploadCoroutineU3Ed__3_System_IDisposable_Dispose_m9B092E9A2C6CC4BFAF7DF5AD15CADFB42E0E578A,
	U3CUploadCoroutineU3Ed__3_MoveNext_m3EAB7574A694091C8460147135112CA52861CC7F,
	U3CUploadCoroutineU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3566843B6A72F6FEAEB8283CE651C40CE72DC81C,
	U3CUploadCoroutineU3Ed__3_System_Collections_IEnumerator_Reset_m6780C1046D1E07C2C66DB71815F6BFA511978ECD,
	U3CUploadCoroutineU3Ed__3_System_Collections_IEnumerator_get_Current_m38D8C3F79A6E04528D70D138A7C69ABBB4CB2907,
};
extern void U3CStartU3Ed__1_MoveNext_m357301D12E426DA23DF15B4B820B249847DD8710_AdjustorThunk (void);
extern void U3CStartU3Ed__1_SetStateMachine_m0A3D213E6669417204AB1327A25A17FA9B1033D2_AdjustorThunk (void);
extern void U3CLoginU3Ed__11_MoveNext_mCE16EF6D49949C97AE4199486E80B1AF860F1455_AdjustorThunk (void);
extern void U3CLoginU3Ed__11_SetStateMachine_m0B507F1355CC9A08B7719EBADF2A26FEC82944E6_AdjustorThunk (void);
extern void U3CCreateNewUserU3Ed__12_MoveNext_m177CF75FA5C9DF2E8F2381350700DE63D1F18644_AdjustorThunk (void);
extern void U3CCreateNewUserU3Ed__12_SetStateMachine_m4258A64835CC6D8DC790485A789CF3550A071F7E_AdjustorThunk (void);
extern void U3CFilterU3Ed__8_MoveNext_m46782994E523B25A90B31D53F18DC6FE3B2C9AA6_AdjustorThunk (void);
extern void U3CFilterU3Ed__8_SetStateMachine_mD5F68C9ECEFC120530129A44C7AE78D164B027DC_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[8] = 
{
	{ 0x06000049, U3CStartU3Ed__1_MoveNext_m357301D12E426DA23DF15B4B820B249847DD8710_AdjustorThunk },
	{ 0x0600004A, U3CStartU3Ed__1_SetStateMachine_m0A3D213E6669417204AB1327A25A17FA9B1033D2_AdjustorThunk },
	{ 0x0600004B, U3CLoginU3Ed__11_MoveNext_mCE16EF6D49949C97AE4199486E80B1AF860F1455_AdjustorThunk },
	{ 0x0600004C, U3CLoginU3Ed__11_SetStateMachine_m0B507F1355CC9A08B7719EBADF2A26FEC82944E6_AdjustorThunk },
	{ 0x0600004D, U3CCreateNewUserU3Ed__12_MoveNext_m177CF75FA5C9DF2E8F2381350700DE63D1F18644_AdjustorThunk },
	{ 0x0600004E, U3CCreateNewUserU3Ed__12_SetStateMachine_m4258A64835CC6D8DC790485A789CF3550A071F7E_AdjustorThunk },
	{ 0x06000052, U3CFilterU3Ed__8_MoveNext_m46782994E523B25A90B31D53F18DC6FE3B2C9AA6_AdjustorThunk },
	{ 0x06000053, U3CFilterU3Ed__8_SetStateMachine_mD5F68C9ECEFC120530129A44C7AE78D164B027DC_AdjustorThunk },
};
static const int32_t s_InvokerIndices[95] = 
{
	2438,
	2112,
	2112,
	2335,
	1545,
	1545,
	2424,
	1545,
	2438,
	1545,
	1545,
	2438,
	2209,
	-1,
	2335,
	2112,
	2112,
	1545,
	582,
	1545,
	1545,
	1545,
	1545,
	1545,
	1503,
	1503,
	1289,
	1545,
	1545,
	1545,
	1545,
	1545,
	1545,
	1545,
	1545,
	1503,
	1289,
	1503,
	1289,
	1503,
	1289,
	1503,
	776,
	1503,
	1545,
	1289,
	1289,
	1289,
	1545,
	1289,
	1289,
	1545,
	1545,
	1545,
	1545,
	1545,
	1545,
	1545,
	1545,
	1545,
	1545,
	1289,
	1545,
	1289,
	1002,
	1545,
	1289,
	1503,
	1289,
	1289,
	1289,
	1503,
	1545,
	1289,
	1545,
	1289,
	1545,
	1289,
	2438,
	1545,
	1289,
	1545,
	1289,
	2438,
	1545,
	1002,
	1002,
	1545,
	1523,
	1279,
	1545,
	1523,
	1503,
	1545,
	1503,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x0600000E, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)3, 9382 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	95,
	s_methodPointers,
	8,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
