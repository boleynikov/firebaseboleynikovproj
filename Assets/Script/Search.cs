using System;

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Search : MonoBehaviour
{
    public event Action<List<Note>> Searching;
 
    [SerializeField] private InputField inputField;
    private string tempText;

    private void Awake()
    {
        tempText = inputField.text;
    }

    public void OnChanged()
    {
        if (!Input.GetKeyDown(KeyCode.Escape))
        {
            tempText = inputField.text;
            Filter();
        }
    }
    public void OnEndEdit()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            inputField.text = tempText;           
            ManagerUI.instance.ClearScrolledContent();
            NotePref.instance.ShowNotes(NetworkControll.instance.User.userNotes);
        }
    }
    private async void Filter()
    {
        ManagerUI.instance.ClearScrolledContent();
        var uid = NetworkControll.instance.Uid;

        var searchTask = inputField.text == "" ?
            FireBasesStruct.Get(uid + "/userNotes") : FireBasesStruct.SearchNote(uid, inputField.text);

        await searchTask;
        var notes = new List<Note>();
        foreach (var item in searchTask.Result.Children)
        {
            var note = FireBasesStruct.Deserialize<Note>(item);
            notes.Add(note);
            NotePref.instance.CreateNotePrefab(note);
        }
        Searching?.Invoke(notes);
    }

}
