using UnityEngine;
using System;


[Serializable]
public class Note 
{
    [SerializeField] private string uid;
    [SerializeField] private string title;
    [SerializeField] private string description;
    [SerializeField] private string date;

    public string Title { get => title; set => title = value; }
    public string Description { get => description; set => description = value; }
    public string Date { get => date; set => date = value; }
    public string Uid => uid;

    public Note(string Title, string Description)
    {
        uid = Guid.NewGuid().ToString();
        date = long.Parse(DateTime.UtcNow.ToString("yyyyMMddHHmmss")).ToString();
        title = Title;
        description = Description;
    }
    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}
