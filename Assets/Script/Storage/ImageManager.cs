using UnityEngine;
using System.IO;
using static NativeGallery;

public class ImageManager : MonoBehaviour
{	
	public Texture2D texture;
	public void PickImage()
	{
		Permission permission = GetImageFromGallery((path) =>
		{
			Debug.Log("Image path: " + path);
			if (path != null)
			{
				texture = new Texture2D(2, 2);
				texture.LoadImage(File.ReadAllBytes(path));
				UploadImage.instance.StartUpload(texture);
			}			
		}, "Select a PNG image", "image/png");
	}
}
