using System;
using System.Collections;
using UnityEngine;
using Firebase.Storage;

public  class UploadImage : MonoBehaviour
{
    public static UploadImage instance;
    
    private void Start()
    {
        instance = this;
    }
    public void StartUpload(Texture2D texture)
    {       
        StartCoroutine(UploadCoroutine(texture));
    }
    /// <summary>
    /// saving only PNG images in folder /images/userID/currentNoteID/imageID
    /// </summary>
    /// <param name="texture"></param>
    /// <returns></returns>
    private IEnumerator UploadCoroutine(Texture2D texture)
    {
        if (Current.FindCurrent() != null)
        {
            FirebaseStorage storage = FirebaseStorage.DefaultInstance;

            StorageReference imageReference = storage.GetReference($"/images/{NetworkControll.instance.User.GetUid()}/{Current.FindCurrent().Uid}/{Guid.NewGuid()}.png");
            var bytes = texture.EncodeToPNG();
            var uploadTask = imageReference.PutBytesAsync(bytes);
            yield return new WaitUntil(() => uploadTask.IsCompleted);

            if (uploadTask.Exception != null)
            {
                Debug.LogError($"Failed to upload:{uploadTask.Exception}");
                yield break;
            }
        }
        else
        {
            Debug.Log("Please, choose note to download image");
        }
    }
}
