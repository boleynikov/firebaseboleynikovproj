using System;
using System.Text;
using System.Threading.Tasks;
using Firebase.Database;
using UnityEngine;
using Firebase;


public static class FireBasesStruct 
{
    public static DatabaseReference reference;
    static FireBasesStruct() 
    {
        reference = FirebaseDatabase.DefaultInstance.RootReference;   
    }
    public static void Combine(out StringBuilder path, params string[] children)
        {
            path = new StringBuilder();
            foreach (var child in children)
            {
                path.Append(child);
                path.Append("/");
            }
            if (path.Length > 0) path.Length -= 1;
            else path.Append("Errors");
        }
    public static T Deserialize<T>(DataSnapshot snapshot) 
    {
        
        return JsonUtility.FromJson<T>(snapshot.GetRawJsonValue());
    }
    public static Task<DataSnapshot> Get(params string[] children)
    {
        Combine(out var path, children);
        return FirebaseDatabase.DefaultInstance.GetReference(path.ToString()).GetValueAsync();
    }

    public static Task Save(string json, params string[] children)
    {
        Combine(out var path, children);
        return reference.Child(path.ToString()).SetRawJsonValueAsync(json);
    }

    public static Task<DataSnapshot> SearchNote(string path, string searchText)
    {
        return reference.Child(path + "/userNotes/").OrderByChild("title").StartAt(searchText).EndAt(searchText + "\uf8ff").GetValueAsync();
    }
}


