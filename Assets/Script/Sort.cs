using UnityEngine;
using System.Linq;

public class Sort : MonoBehaviour
{
    /// <summary>
    /// sort function by click title button
    /// </summary>
    public void OrderByTitle()
    {
        Debug.Log("Try to sort by title");
        ManagerUI.instance.ClearScrolledContent();
        var sorted = from t in NetworkControll.instance.User.userNotes
                     orderby t.Title
                     select t;
        NotePref.instance.ShowNotes(sorted);
    }    
    /// <summary>
    /// sort function by click date button
    /// </summary>
    public void OrderByDate()
    {
        Debug.Log("Try to sort by date");
        ManagerUI.instance.ClearScrolledContent();
        var sorted = from t in NetworkControll.instance.User.userNotes
                     orderby t.Date
                     select t;
        NotePref.instance.ShowNotes(sorted);
    }
    
}
