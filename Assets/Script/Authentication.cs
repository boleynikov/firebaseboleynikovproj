using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Auth;
using UnityEngine;
using static Firebase.Auth.FirebaseAuth;

public static class Authentication
{
    private static FirebaseAuth auth;

    public static void InitFirebase()
    {
        auth = FirebaseAuth.DefaultInstance;
    }
    public static Task<FirebaseUser> SignUp(string email, string password)
    {

        return auth.CreateUserWithEmailAndPasswordAsync(email, password);
    }

    public static Task<FirebaseUser> SignIn(string email, string password)
    {
        return auth.SignInWithEmailAndPasswordAsync(email, password);
    }

    public static Task ResetPassword(string email)
    {
        return auth.SendPasswordResetEmailAsync(email);
    }
}
