using UnityEngine;
using UnityEngine.UI;

public class ManagerUI : MonoBehaviour
{
    public static ManagerUI instance;
 
    public InputField entryName;
    public InputField entryPassword;
    public InputField titleNoteEntry;
    public InputField descriptionNoteEntry;

    [SerializeField] private Transform ScrolledContent;

    [SerializeField] private GameObject inputMenu;
    [SerializeField] private GameObject noteButton;

    [SerializeField] private GameObject SortButton;
    [SerializeField] private GameObject SortDialog;
    [SerializeField] private GameObject AddImageButton;

    [SerializeField] private GameObject EditButton;
    [SerializeField] private GameObject DeleteButton;

    [SerializeField] private GameObject inputForNote;


    private bool sorted = true;
   

    void Start()
    {
        if (instance == null)
        {
            instance = this;           
        }else if (instance != null)
        {            
            Destroy(this);
        }
    }
    public bool GetWritten(out string str, InputField Field)
    {
        if(Field.text != "")
        {
            str = Field.text;
            return true;
        }
        else
        {
            str = "";
            return false;
        }
    }
    public void FromLoginToNotes()
    {
        inputMenu.SetActive(false);
        inputForNote.SetActive(true);
        noteButton.SetActive(true);
        AddImageButton.SetActive(true);
    }
    public void ToSort()
    {
        if (sorted)
        {
            SortDialog.SetActive(true);
            sorted = false;
        }
        else
        {
            SortDialog.SetActive(false);
            sorted = true;
        }
    }
    
    public void ClearScrolledContent()
    {
        foreach(Transform child in ScrolledContent)
        {
            Destroy(child.gameObject);
        }
    }

    public void Exit()
    {
        Application.Quit();
    }
}
