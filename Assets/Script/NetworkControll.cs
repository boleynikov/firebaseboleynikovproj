using UnityEngine;
using System;


public class NetworkControll : MonoBehaviour
{
    public static NetworkControll instance;

    private User user;
    private string email;
    private string password;
    private string uid;
    public User User => user;
    public string Uid { get => uid; private set => uid = value; }

    private void Awake()
    {
        instance = this;
        Authentication.InitFirebase();
    }
    public async void Login()
    {       
        if (ManagerUI.instance.GetWritten(out email, ManagerUI.instance.entryName) && 
            ManagerUI.instance.GetWritten(out password, ManagerUI.instance.entryName))
        {
            var authTask = Authentication.SignIn(email, password);
            await authTask;
            Uid = authTask.Result.UserId;
            var userTask = FireBasesStruct.Get(Uid);
            await userTask;
            user = FireBasesStruct.Deserialize<User>(userTask.Result);
            ManagerUI.instance.FromLoginToNotes();
            NotePref.instance.ShowNotes(user.userNotes);
            Debug.Log("singIn");
        }
        else     
            Debug.Log("Please, enter your valid email or password");       
    }
    public async void CreateNewUser()
    {
        if (ManagerUI.instance.GetWritten(out email, ManagerUI.instance.entryName) &&
            ManagerUI.instance.GetWritten(out password, ManagerUI.instance.entryName))
        {
            var createUserTask = Authentication.SignUp(email, password);
            await createUserTask;
            Uid = createUserTask.Result.UserId;           

            user = new User(Uid);
            Debug.Log("singUp");
            UpdateData();
            ManagerUI.instance.FromLoginToNotes();
        }
        else
            Debug.Log("Please, enter your User name");      
    }
    public void CreateNote()
    {
        string title, description;
        if (ManagerUI.instance.GetWritten(out title, ManagerUI.instance.titleNoteEntry) && 
            ManagerUI.instance.GetWritten(out description, ManagerUI.instance.descriptionNoteEntry)) 
        {
            Note note = new Note(title, description);            
            FireBasesStruct.reference.Root.Child(Uid).Child("userNotes").Child(user.userNotes.Count.ToString()).SetRawJsonValueAsync(note.ToString());
            user.AddNote(note);
            NotePref.instance.CreateNotePrefab(note);
        }
    }
    public void EditNote()
    {
        string title, description;
        if (ManagerUI.instance.GetWritten(out title, ManagerUI.instance.titleNoteEntry) |
            ManagerUI.instance.GetWritten(out description, ManagerUI.instance.descriptionNoteEntry)) {
            Note note = Current.FindCurrent();
            note.Title = title == "" ? note.Title: title;
            note.Description = description == "" ? note.Description : description;
            note.Date = long.Parse(DateTime.UtcNow.ToString("yyyyMMddHHmmss")).ToString();
            user.EditNote(note);
            UpdateData();
            ManagerUI.instance.ClearScrolledContent();
            NotePref.instance.ShowNotes(user.userNotes);
        }
    }
    public void DeleteNote()
    {
        user.DeleteNote(Current.FindCurrent());
        UpdateData();
        ManagerUI.instance.ClearScrolledContent();
        NotePref.instance.ShowNotes(user.userNotes);
    }
    public void UpdateData()
    {
        FireBasesStruct.Save(user.ToString(), Uid).ContinueWith(t =>
        {
            if (t.IsFaulted)
                Debug.Log("Updating failed");
            else
                Debug.Log("Updating success");
        });
    }   
}
