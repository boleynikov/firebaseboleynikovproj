using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Current : MonoBehaviour
{ 
    public static bool current = true;
    private static string CurrentNoteUid;
    private Color DefaultColor;

    private void Start()
    {
        DefaultColor = GetComponent<Image>().color;
    }
    public void Click()
    {
        if (current)
        {
            CurrentNoteUid = gameObject.name;
            gameObject.GetComponent<Image>().color = Color.gray;
            ManagerUI.instance.titleNoteEntry.text = gameObject.GetComponentInChildren<Text>().text;
            ManagerUI.instance.descriptionNoteEntry.text = gameObject.GetComponentInChildren<TextMeshProUGUI>().text;
            current = false;
        }
        else
        {
            CurrentNoteUid = null;
            gameObject.GetComponent<Image>().color = DefaultColor;
            ManagerUI.instance.titleNoteEntry.text = "";
            ManagerUI.instance.descriptionNoteEntry.text = "";
            current = true;
        }
    }
    public static Note FindCurrent() 
    { 
        foreach(var t in NetworkControll.instance.User.userNotes)
        {
            if(t.Uid == CurrentNoteUid)            
                return t;               
        }
        return null;
    }
}
