using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;

public class NotePref : MonoBehaviour
{
    public static NotePref instance;
    [SerializeField] GameObject noteHolder;
    
    [SerializeField] Button notePrefab;
    [SerializeField] Text notePrefabTitle;
    [SerializeField] TextMeshProUGUI notePrefabDescription;
    
    private void Start()
    {
        instance = this;
    }

    public void CreateNotePrefab(Note note)
    {
        notePrefabTitle = notePrefab.GetComponentInChildren<Text>();
        notePrefabTitle.text = note.Title;
        notePrefabDescription = notePrefab.GetComponentInChildren<TextMeshProUGUI>();
        notePrefabDescription.text = note.Description;
        Button newNote = Instantiate(notePrefab, noteHolder.transform);
        newNote.name = note.Uid;
        newNote.gameObject.SetActive(true);
        
    }
    public void ShowNotes(List<Note> noteList)
    { 
        foreach (Note note in noteList)
        {
            CreateNotePrefab(note);
        }
    }
    /// <summary>
    /// overload for using after LINQ function implementing
    /// </summary>
    /// <param name="noteList"></param>
    public void ShowNotes(IOrderedEnumerable<Note> noteList)
    {
        foreach (var note in noteList)
        {
            CreateNotePrefab(note);
        }
    }

   
}
