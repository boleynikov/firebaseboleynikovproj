using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class User
{
    [SerializeField] private string uid;
    public List<Note> userNotes;    
    public User(string uid)
    {
        this.uid = uid;
        userNotes = new List<Note>();
    }
    public string GetUid()
    {
        return uid;
    }
    public void AddNote(Note newNote)
    {
        userNotes.Add(newNote);
    }
    public void EditNote(Note note)
    {
        var index = userNotes.IndexOf(note);
        userNotes[index] = note;
    }

    public void DeleteNote(Note note)
    {
        userNotes.Remove(note);
    }
    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }

}
