using Firebase;
using UnityEngine;
using UnityEngine.Events;

public class FireBaseInit : MonoBehaviour
{  
    public UnityEvent OnFirebaseInitialized = new UnityEvent();
     async void Start()
    {
        var dependencyStatus = await FirebaseApp.CheckAndFixDependenciesAsync();
        if (dependencyStatus == DependencyStatus.Available)
        {
            OnFirebaseInitialized.Invoke();
        }
    }
}
